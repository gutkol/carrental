﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarRental.Models
{
    public class RentalViewModel
    {
        public int IDMiejsceWyp { get; set; }
        public int IDMiejsceOdd { get; set; }
        [DataType(DataType.Date)]
        public DateTime DataWyp { get; set; }
        [DataType(DataType.Date)]
        public DateTime DataOdd { get; set; }
        public bool ReturnPlace { get; set; }
    }
}